# Change log #
*This game is still experimental, so there are tons of bugs*

### v.0.9 ###
* added system.cls command to display only one grid
### v.0.8 ###
* fixed Frodo disappear when get closer to rider
* fixed new level loop
* add ability to generate up to 30 riders
* remove rider follow Frodo feature due to too much bugs 
### v.0.7 ###
* add end game loop
* improve some coding
* next level still have tons of bugs [experimental]
### v.0.6 ###
* end game check if Frodo had caught by riders (not working)
* more bug than older version!!!!!!!!!!!!!!!!!!!!!!!!
### v.0.5 ###
* end game check if Frodo can get to exit
### v.0.4 ###
* bug fixes in moving rider will not be at end point
### v.0.3 ###
* bug fixes in generating rider
### v.0.2 ###
* ability to generate rider 
* levels counter with riderquantity
### V.0.1 ###
* ability to move Frodo
* prints grid