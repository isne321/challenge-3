#include<iostream>
#include<ctime>
#include<cstdlib>

using namespace std;

const int gridSize = 10;
void printGrid(char grid[][gridSize], int); //print grid
void moveFrodo(char grid[][gridSize]); //user moves frodo
void riderCreate(char grid[][gridSize], int riderPos[30][2], int, int&); //gen. new riders
int riderQuantityCheck(int); //check how many riders
void riderMove(char grid[][gridSize], int riderPos[30][2], int); //gen. movement of riders
void endGameCheck(char grid[][gridSize], int riderPos[30][2], int, int&, bool&); //check end game
void newGame(char grid[][gridSize], int riderPos[30][2], int, bool&, int&); //to create new game after win/lose

int rowPos = 0, colPos = 0; //x,y position of Frodo

int main() {
	char grid[gridSize][gridSize] = { };
	grid[9][9] = { 'E' };
	grid[0][0] = { 'F' };
	int riderPos[30][2] = {}; //array to store x,y position 
	int level = 1;

	bool gameStatus = true;
	int riderQuantity = riderQuantityCheck(level);
	riderCreate(grid, riderPos, riderQuantity, level); 
	while (level>0)
	{

		while (gameStatus == true) {
			riderQuantity = riderQuantityCheck(level);
			//system("cls");
			printGrid(grid, level);
			moveFrodo(grid);
			riderMove(grid, riderPos, riderQuantity);
			grid[rowPos][colPos] = { 'F' }; //make sure frodo won't disappear
			grid[9][9] = { 'E' }; //make sure exit won't disappear
			endGameCheck(grid, riderPos, riderQuantity, level, gameStatus);
		}

		if (level == 0){
			int input;
			cout << "Play again? [1 : play again | 0 : exit]  " << endl;
			cin >> input;

			while (input != 0 && input != 1) {
				cout << "invalid input" << endl;
				cin >> input;
			}

			if (input == 1) {
				
				level = 1;
				riderQuantity = 3;
				newGame(grid, riderPos, riderQuantity, gameStatus, level);
			} else {
				break; //exit
			}
		}
		if (level > 1) {

			newGame(grid, riderPos, riderQuantity, gameStatus, level);
			grid[9][9] = { 'E' }; //make sure exit won't disappear
			//level++;
		}
	}
	return 0;
}
void newGame(char grid[gridSize][gridSize], int riderPos[30][2], int riderQuantity, bool& gameStatus, int& level) {
	gameStatus = true;

	for (int j = 0; j < gridSize; j++) {
		for (int k = 0; k < gridSize; k++) {
			grid[j][k] = { ' ' };
		}
	}
	for (int i = 0; i < 30; i++) {
		riderPos[i][0] = { 0 };
		riderPos[i][1] = { 0 };
	}
	grid[0][0] = { 'F' };
	grid[9][9] = { 'E' };
	grid[rowPos][colPos] = { ' ' };
	rowPos = 0;
	colPos = 0;
	riderCreate(grid, riderPos, riderQuantity, level);


}
void endGameCheck(char grid[gridSize][gridSize], int riderPos[30][2], int riderQuantity, int& level, bool& gameStatus) {
	//bool gameStatus = true;
	if (rowPos == 9 && colPos == 9) {
		cout << "Frodo makes it!" << endl;
		gameStatus = false;
		level += 1;
		//cout << level << endl;
	} else {
		for (int i = 0; i < riderQuantity; i++) {
			if (riderPos[i][0] == rowPos && riderPos[i][1] == colPos) {
				//system("cls");
				grid[riderPos[i][0]][riderPos[i][0]] = { ' ' };
				grid[rowPos][colPos] = { 'X' };
				cout << "Frodo caught by Riders!" << endl;
				gameStatus = false;
				printGrid(grid, level);
				level = 0;
			}
		}
	}
}
void riderMove(char grid[gridSize][gridSize], int riderPos[30][2], int riderQuantity) {
	srand(time(0));
	
	for (int i = 0; i < riderQuantity; i++) {
		//while ((riderPos[i][0] != 9 && riderPos[i][1] != 9) || (riderPos[i][0] != 0 && riderPos[i][1] != 0)) {

		do {
			int genMoveRow = (rand() % 2), genMoveCol = (rand() % 2);
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			if (genMoveCol == 0 && riderPos[i][1] + 1 <= 9) {
				//if ((riderPos[i][1] + 1 != 9 && riderPos[i][0] != 9) && (riderPos[i][1] + 1 != 0 && riderPos[i][0] != 0)) {
				if (grid[riderPos[i][0]][riderPos[i][1] + 1] = { ' ' }) {
					riderPos[i][1] += 1;
				}
				//}
			}
			else if (genMoveCol == 1 && riderPos[i][1] - 1 >= 0) {
				//if ((riderPos[i][1] - 1 != 9 && riderPos[i][0] != 9) && (riderPos[i][1] - 1 != 0 && riderPos[i][0] !=0) ) {
				if (grid[riderPos[i][0]][riderPos[i][1] - 1] = { ' ' }) {
					riderPos[i][1] -= 1;
				}
				//}
			}

			if (genMoveRow == 0 && riderPos[i][0] + 1 <= 9) {
				//if ((riderPos[i][1] != 9 && riderPos[i][0] + 1 != 9) && (riderPos[i][1] != 0 && riderPos[i][0] + 1 != 0)) {
				if (grid[riderPos[i][0] + 1][riderPos[i][1]] = { ' ' }) {
					riderPos[i][0] += 1;
				}
				//}
			}
			else if (genMoveRow == 1 && riderPos[i][0] - 1 >= 0) {
				//if ((riderPos[i][1] != 9 && riderPos[i][0] - 1 != 9) && (riderPos[i][1] != 0 && riderPos[i][0] - 1 != 0) ) {
				if (grid[riderPos[i][0] - 1][riderPos[i][1]] = { ' ' }) {
					riderPos[i][0] -= 1;
				}
				//}
			}
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		} while (grid[riderPos[i][0]][riderPos[i][1]] == grid[9][9] 
			&& grid[riderPos[i][0]][riderPos[i][1]] == grid[0][0]);
		//}
		//Rider follows if they can see Frodo
		/*
		if (riderPos[i][0] + 1 == rowPos && riderPos[i][1] == colPos) { //down
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0] += 1;
			riderPos[i][1];
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] + 1 == rowPos && riderPos[i][1] - 1 == colPos) { // lower left
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0] += 1;
			riderPos[i][1] -= 1;
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] + 1 == rowPos && riderPos[i][1] + 1 == colPos) { // lower right
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0] += 1;
			riderPos[i][1] += 1;
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] == rowPos && riderPos[i][1] + 1 == colPos) { // right
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0];
			riderPos[i][1] += 1;
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] == rowPos && riderPos[i][1] - 1 == colPos) { // left
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0];
			riderPos[i][1] -= 1;
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] - 1 == rowPos && riderPos[i][1] == colPos) { // up
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0] -= 1;
			riderPos[i][1];
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] - 1 == rowPos && riderPos[i][1] - 1 == colPos) { // upper left
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0] -= 1;
			riderPos[i][1] -= 1;
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		if (riderPos[i][0] - 1 == rowPos && riderPos[i][1] + 1 == colPos) { // upper right
			grid[riderPos[i][0]][riderPos[i][1]] = { ' ' };
			riderPos[i][0] -= 1;
			riderPos[i][1] += 1;
			grid[riderPos[i][0]][riderPos[i][1]] = { 'R' };
		}
		else {*/
		//Generate move


	}
	//Rider Position Check
	
	for (int j = 0; j < 10; j++) {
	for (int k = 0; k < 2; k++) {
	cout << riderPos[j][k] << " ";
	}
	cout << endl;
	}
	cout << endl;
	
	
	
}
int riderQuantityCheck(int level) {
	return level * 3;
}
void riderCreate(char grid[gridSize][gridSize], int riderPos[30][2], int riderQuantity, int& level) {
	riderQuantity = riderQuantityCheck(level);

	srand(time(0));
	for (int i = 0; i < riderQuantity; i++) {
		bool loop = true;
		int rowRider = (rand() % 6 + 2), colRider = (rand() % 6 + 2);
		while (loop == true) {
			if (colRider >= 2 && colRider <= 7 && rowRider >= 2 && rowRider <= 7) {
				riderPos[i][0] = { rowRider };
				riderPos[i][1] = { colRider };
				grid[rowRider][colRider] = { 'R' };
				loop = false;
			}
			else {
				rowRider = (rand() % 6 + 2);
				colRider = (rand() % 6 + 2);
				loop = true;
			}
		}
		loop = true;

		
	}
	/*
	//Rider Position Check
	for (int j = 0; j < 10; j++) {
		for (int k = 0; k < 2; k++) {
			cout << riderPos[j][k] << " ";
		}
		cout << endl;
	}
	cout << endl;
	*/
}
void moveFrodo(char grid[gridSize][gridSize]) {
	int move = 0;
	cin >> move;
	if (move >= 1 && move <= 9) {
		if (move == 1 && colPos - 1 >= 0 && rowPos + 1 <= 9) { //lower left
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos + 1][colPos - 1] = { 'F' };
			rowPos++;
			colPos--;
		}
		else if (move == 2 && rowPos + 1 <= 9) { //down
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos + 1][colPos] = { 'F' };
			rowPos++;
		}
		else if (move == 3 && colPos + 1 <= 9 && rowPos + 1 <= 9) { //lower right
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos + 1][colPos + 1] = { 'F' };
			rowPos++;
			colPos++;
		}
		else if (move == 4 && colPos - 1 >= 0) { //left
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos][colPos - 1] = { 'F' };
			colPos--;
		}
		else if (move == 6 && colPos + 1 <= 9) { //right
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos][colPos + 1] = { 'F' };
			colPos++;
		}
		else if (move == 7 && colPos - 1 >= 0 && rowPos - 1 >= 0) { //upper left
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos - 1][colPos - 1] = { 'F' };
			rowPos--;
			colPos--;
		}
		else if (move == 8 && rowPos - 1 >= 0) { //up
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos - 1][colPos] = { 'F' };
			rowPos--;
		}
		else if (move == 9 && colPos + 1 <= 9 && rowPos - 1 >= 0) { //upper right
			grid[rowPos][colPos] = { ' ' };
			grid[rowPos - 1][colPos + 1] = { 'F' };
			rowPos--;
			colPos++;
		}
	}
}
void printGrid(char grid[gridSize][gridSize], int level) {

	/*
	//Current Frodo Position
	cout << rowPos << "," << colPos << endl;
	*/

	cout << " Frodo & The Lord of the Rings Game! " << endl;
	cout << " level " << level << endl;
	cout << " -----------------------------------------" << endl;
	for (int i = 0; i < gridSize; i++) {
		cout << " | ";
		for (int j = 0; j < gridSize; j++) {
			cout << grid[i][j] << " | ";
		}
		cout << endl;
		cout << " -----------------------------------------" << endl;
	}
	cout << " Make sure that the riders are dangerous,\n Sometime they stick together in one cell. " << endl;
}